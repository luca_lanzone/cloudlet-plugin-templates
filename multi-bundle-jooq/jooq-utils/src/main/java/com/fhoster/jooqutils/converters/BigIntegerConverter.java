package com.fhoster.jooqutils.converters;

import java.math.BigInteger;
import org.jooq.impl.AbstractConverter;
import org.jooq.types.ULong;

@SuppressWarnings("all")
public class BigIntegerConverter extends AbstractConverter<ULong, BigInteger> {
	public BigIntegerConverter() {
		super(ULong.class, BigInteger.class);
	}

	@Override
	public BigInteger from(final ULong databaseObject) {
		if (null == databaseObject) {
			return null;
		}
		return new BigInteger(databaseObject.toString());
	}

	@Override
	public ULong to(final BigInteger userObject) {
		if (null == userObject) {
			return null;
		}
		return ULong.valueOf(userObject);
	}
}
