package com.fhoster.jooqutils;

import java.math.BigInteger;

public interface IdProvider {
	BigInteger get();
}
