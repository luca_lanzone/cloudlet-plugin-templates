#!/bin/bash
which docker

if [ $? -eq 0 ]
then
    docker --version | grep "Docker version"
    if [ $? -eq 0 ]
    then
        echo "[LB] OK docker existing"
    else
        echo "[LB] Installing Docker"
    fi
else
    echo "[LB] Installing Docker" >&2
fi

docker-compose up -d

CONTAINER_ID=$(docker ps | grep dbms-test | awk '{print $1}')

export DB_HOST=$(docker inspect $CONTAINER_ID | grep IPAddress\": | tail -1 | awk '{print $2}' | sed 's/\"//g' | sed 's/,//g')
export DB_PORT=$(docker inspect $CONTAINER_ID| grep tcp | tail -1 | sed 's/\/tcp\": \[//g' | sed 's/"//g' | awk '{print $1}')

sed -i "s/DB_HOST=[^ ]*/DB_HOST=$DB_HOST/g" gradle.properties
sed -i "s/DB_PORT=[^ ]*/DB_PORT=$DB_PORT/g" gradle.properties

echo "[LB] Exporting the following environment variables and gradle properties:"
echo DB_HOST=$DB_HOST
echo DB_PORT=$DB_PORT



